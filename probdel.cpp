﻿#include <iostream>
#include <vector>
using namespace std;

int sledDel(unsigned int d)
{
    while (true)
    {
        int c = 1;
        ++d;
        for (int i = 2; i <= d; ++i)
            if (d % i == 0)
                ++c;
        if (c == 2)
            return d;
    }
}

int main()
{
    setlocale(LC_ALL, "Russian");

    unsigned int n, b;
    cout << "Введите n:    ";
    cin >> n;

    if (n <= 1) {
        cout << "\nВведите другое n";
        return 0;
    }

    cout << "Введите границу B:    ";
    cin >> b;
    int r, q, t = 0;
    bool m = true;
    int d = 2;

    vector <int> p;

    while (n != 1) {
        r = n % d;
        q = n / d;

        if (r == 0) {
            p.push_back(d);
            n = q;
        }
        else if (q > d) {
            d = sledDel(d);
            if (d > b) {
                m = false;
                break;
            }
        }
        else {
            p.push_back(n);
            break;
        }
    }

    if (m)
        cout << "Факторизация полная" << endl;
    else
        cout << "Факторизация неполная, остаток: " << n << endl;

    std::copy(p.begin(), p.end(), std::ostream_iterator<int>(std::cout, " "));
    return 0;
}